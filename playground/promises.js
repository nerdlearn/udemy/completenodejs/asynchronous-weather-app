var asyncAdd = (a, b) => {
	return new Promise((resolve, reject) =>{
		setTimeout(() =>{
			if(typeof a === 'number' && typeof b === 'number'){
				resolve(a+b);
			}else{
				reject('Both values must be numbers.');
			}
		}, 1500);
	});
}

asyncAdd(5,7).then((res) =>{
	console.log('Result', res);
	return asyncAdd(res,33);
}, (errorMessage)=>{
	console.log(errorMessage);
}).then((res)=>{
	console.log('Result should be 45', res);
}, (errorMessage)=>{
	console.log(errorMessage);
});


//Because errors are 'handled' the promise executes the THEN, generating bad behavior "Result should be 45x undefined"
asyncAdd(5,'a').then((res) =>{
	console.log('Result', res);
	return asyncAdd(res,33);
}, (errorMessage)=>{
	console.log('x1: ',errorMessage);
}).then((res)=>{
	console.log('Result should be 45x', res);
}, (errorMessage)=>{
	console.log('x2: ',errorMessage);
});

//We use catch to handle all errors for promise chaining in one place
asyncAdd(5,7).then((res) =>{
	console.log('Result', res);
	return asyncAdd(res,33);
}).then((res)=>{
	console.log('Result should be 45a', res);
}).catch((errorMessage)=>{
	console.log('a: ',errorMessage);
});

asyncAdd(5,'a').then((res) =>{
	console.log('Result', res);
	return asyncAdd(res,33);
}).then((res)=>{
	console.log('Result should be 45b', res);
}).catch((errorMessage)=>{
	console.log('b: ',errorMessage);
});

var somePromise = new Promise((resolve, reject) => {
	setTimeout(()=>{
		//resolve("Hey, it worked!");
		reject("Unable to fulfill promise");
	},2500);
});

somePromise.then((message) => {
	console.log('Success: ', message);
}, (errorMessage) => {
	console.log('Failure: ', errorMessage);
});