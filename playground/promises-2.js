require('dotenv').config();
console.log('Starting geocode.js');
const request = require('request');
const GOOGLE_API_URL = process.env.GOOGLE_API_URL || 'https://maps.googleapis.com/maps/api/geocode/json'; //obtain the API for the service
const GOOGLE_API_KEY = process.env.GOOGLE_API_KEY; //obtain the API KEY for the service


var geocodeAddress = (addressReceived) => {
		
	return new Promise((resolve, reject) =>{
		console.log('GOOGLE_API_URL = ', GOOGLE_API_URL);
		console.log('GOOGLE_API_KEY = ', GOOGLE_API_KEY);
		console.log('addressReceived = ', addressReceived);
		const encodedAddressReceived = encodeURIComponent(addressReceived);

		console.log('encodedAddressReceived = ', encodedAddressReceived);
		
		var unifiedUrl = GOOGLE_API_URL + '?address=' + encodedAddressReceived + '&key=' + GOOGLE_API_KEY

		console.log('unifiedUrl = ', unifiedUrl);

		request({
		  url:unifiedUrl,
		  json: true
		}, (error, response, body) => {
		  console.log('Request was made, we are handling response.');
		  console.log('response.statusCode', response.statusCode);
		  if(error){
			reject(`Unable to connect. Error thrown: ${error}`);
		  } else if (response.statusCode === 400){
			reject('400 error, INVALID_REQUEST", our body received was: ', JSON.stringify(body, undefined, 2));
		  } else if (response.statusCode === 404){
			reject(`404 error, check connection string.`);
		  } else if (response.statusCode === 200){
			console.log(`response.statusCode === 200, lets see what we got.`);
			console.log('body.status is: ', body.status);
			if (body.status === 'ZERO_RESULTS'){
			  reject(`Invalid address, no results obtained. ZERO_RESULTS`);
			} else if (body.status === 'OK') {
			  console.log('body.status is OK, lets see what we got as a result');
			  resolve({
				  address: body.results[0].formatted_address,
				  latitude: body.results[0].geometry.location.lat, 
				  longitude: body.results[0].geometry.location.lng
			  });
			}
		  }
		});
	});
};

geocodeAddress('19146').then((location)=>{
	console.log(JSON.stringify(location, undefined, 2));
},(errorMessage)=>{
	console.log(errorMessage);
});a

module.exports = {
	geocodeAddress
}