require('dotenv').config();
const yargs = require('yargs');
const axios = require('axios');
const GOOGLE_API_URL = process.env.GOOGLE_API_URL || 'https://maps.googleapis.com/maps/api/geocode/json'; //obtain the API for the service
const GOOGLE_API_KEY = process.env.GOOGLE_API_KEY; //obtain the API KEY for the service
const DARKSKY_API_URL = process.env.DARKSKY_API_URL || 'https://maps.googleapis.com/maps/api/geocode/json'; //obtain the API for the service
const DARKSKY_API_KEY = process.env.DARKSKY_API_KEY; //obtain the API KEY for the service

const argv = yargs
  .options({
    address: {
      describe: 'Address to look up',
      demand: true, 
      alias: 'a',
      string: true
    }
  })
  .help()
  .alias('help', 'h')
  .argv;
  
console.log('yargs.argv: ',argv);

console.log('GOOGLE_API_URL = ', GOOGLE_API_URL);
console.log('GOOGLE_API_KEY = ', GOOGLE_API_KEY);
console.log('addressReceived = ', argv.address);
const encodedAddressReceived = encodeURIComponent(argv.address);

console.log('encodedAddressReceived = ', encodedAddressReceived);

var unifiedUrl = GOOGLE_API_URL + '?address=' + encodedAddressReceived + '&key=' + GOOGLE_API_KEY

console.log('unifiedUrl = ', unifiedUrl);

axios.get(unifiedUrl).then((response)=>{
	if (response.data.status === 'ZERO_RESULTS'){
		throw new Error('Unable to find that address');
	}
	var latitude = response.data.results[0].geometry.location.lat;
	var longitude = response.data.results[0].geometry.location.lng;
	var formattedAddress = response.data.results[0].formatted_address;
	
	console.log('DARKSKY_API_URL = ', DARKSKY_API_URL);
	console.log('DARKSKY_API_KEY = ', DARKSKY_API_KEY);
	console.log('latitude = ', latitude);
	console.log('longitude = ', longitude);
	
	var unifiedWeatherUrl = DARKSKY_API_URL + '/' + DARKSKY_API_KEY + '/' + latitude + ',' + longitude;

	console.log('unifiedWeatherUrl = ', unifiedWeatherUrl);
		
	console.log(response.data);
	
	return axios.get(unifiedWeatherUrl);
}).then((response)=>{
	var temperature = response.data.currently.temperature;
	var apparentTemperature = response.data.currently.apparentTemperature;
	
	console.log(`It's currently ${temperature} but it feels like ${apparentTemperature}`);
}).catch((e)=>{
	if(e.code === 'ENOTFOUND'){
		console.log('Unable to connect to API servers');
	} else if (e.response.status = 404){
		console.log(`404 error, check connection string.`);
	} else if (e.response.status = 400){
		console.log('400 error, INVALID_REQUEST", our body received was: ', JSON.stringify(e.response.body, undefined, 2));
	} else {
		console.log('We have an unspecified Error: ', e.message);
		console.log('We have an unspecified Error: ', JSON.stringify(e, undefined, 2));
	}
})



