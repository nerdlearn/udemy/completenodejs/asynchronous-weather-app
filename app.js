require('dotenv').config();
const yargs = require('yargs');
const geocode = require('./geocode/geocode.js');
const weather = require('./weather/weather.js');


const argv = yargs
  .options({
    address: {
      describe: 'Address to look up',
      demand: true, 
      alias: 'a',
      string: true
    }
  })
  .help()
  .alias('help', 'h')
  .argv;
  
console.log('yargs.argv: ',argv);

/*geocode.geocodeAddress(argv.address, (errorMessage, results) => {
	if(errorMessage){
		console.log(errorMessage);
	} else {
		console.log(JSON.stringify(results, undefined, 2));
	}
});*/
const latitude = '10.0144716';
const longitude = '-84.0986459';

weather.getWeather(latitude, longitude, (errorMessage, results) => {
	if(errorMessage){
		console.log(errorMessage);
	} else {
		console.log(JSON.stringify(results, undefined, 2));
	}
});
  