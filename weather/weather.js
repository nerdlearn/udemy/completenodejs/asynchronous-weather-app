require('dotenv').config();
console.log('Starting weather.js');
const request = require('request');
const DARKSKY_API_URL = process.env.DARKSKY_API_URL || 'https://maps.googleapis.com/maps/api/geocode/json'; //obtain the API for the service
const DARKSKY_API_KEY = process.env.DARKSKY_API_KEY; //obtain the API KEY for the service


var getWeather = (latitude, longitude, callback) => {

	console.log('DARKSKY_API_URL = ', DARKSKY_API_URL);
	console.log('DARKSKY_API_KEY = ', DARKSKY_API_KEY);
	console.log('latitude = ', latitude);
	console.log('longitude = ', longitude);
	
	try{

		var unifiedUrl = DARKSKY_API_URL + '/' + DARKSKY_API_KEY + '/' + latitude + ',' + longitude;

		console.log('unifiedUrl = ', unifiedUrl);

		request({
		  url:unifiedUrl,
		  json: true
		}, (error, response, body) => {
		  console.log('Request was made, we are handling response.');
		  console.log('response.statusCode', response.statusCode);
		  if(error){
			callback(`Unable to connect. Error thrown: ${error}`);
		  } else if (response.statusCode === 400){
			callback('400 error, INVALID_REQUEST", our body received was: ', JSON.stringify(body, undefined, 2));
		  } else if (response.statusCode === 404){
			callback(`404 error, check connection string.`);
		  } else if (response.statusCode === 200){
			console.log(`response.statusCode === 200, lets see what we got.`);
		    console.log('body.status is OK, lets see what we got as a result');
		    console.log('body.currently.temperature', body.currently.temperature);
		    callback( undefined, {
			    temperature: body.currently.temperature,
		    });
		  }
		});
	} catch(e){
		console.log('Ooops, something went wrong at getWeather with error: ', e);
	}
};



module.exports = {
	getWeather
}
